import Visible from "@material-ui/icons/VisibilityOutlined";
import Add from "@material-ui/icons/AddCircleOutlineOutlined";
import ViewAll from "views/UserSegment/ViewAll";
import Uedit from "views/UserSegment/Uedit.js";
import Udelete from "views/UserSegment/Udelete.js";
import Ucreate from "views/UserSegment/Ucreate.js";
import Ublock from "views/UserSegment/Ublock.js";

import ResetPwd from "views/UserSegment/ResetPwd.js";
const dashboardRoutes = [
  {
    path: "/useruserview",
    name: "Users",
    rtlName: "",
    icon: "",
    component: ViewAll,
    layout: "/admin",
  },

  {
    path: "/useruserview",
    name: "View All",
    rtlName: "User Segment",
    icon: Visible,
    component: ViewAll,
    layout: "/admin",
  },

  {
    path: "/ucreate",
    name: "Create",
    rtlName: "User Segment/create",
    icon: Add,
    component: Ucreate,
    layout: "/admin",
  },

  {
    path: "/udelete",
    name: "",
    rtlName: "User Segment/delete",
    icon: "",
    component: Udelete,
    layout: "/admin",
  },
  {
    path: "/uedit",
    name: "",
    rtlName: "User Segment/edit",
    icon: "",
    component: Uedit,
    layout: "/admin",
  },
  {
    path: "/ublock",
    name: "",
    rtlName: "User Segment/block",
    icon: "",
    component: Ublock,
    layout: "/admin",
  },
  {
    path: "/resetpwdd",
    name: "",
    rtlName: "Reset Password",
    icon: "",
    component: ResetPwd,
    layout: "/admin",
  },
];

export default dashboardRoutes;
