import Visible from "@material-ui/icons/VisibilityOutlined";
import Add from "@material-ui/icons/AddCircleOutlineOutlined";
import Delete from "@material-ui/icons/DeleteOutlineOutlined";
import Edit from "@material-ui/icons/EditOutlined";
import Stars from "@material-ui/icons/Stars";
import Tier1 from "views/Tier/Tier1.js";
import Tedit from "views/Tier/Tedit.js";
import Tcreate from "views/Tier/Tcreate.js";
import Tdelete from "views/Tier/Tdelete.js";
import Tview from "views/Tier/Tview";

const dashboardRoutes = [
  {
    path: "/tier1",
    name: "Tier",
    rtlName: "Tier",
    icon: Stars,
    component: Tier1,
    layout: "/admin",
  },

  {
    path: "/tierview",
    name: "View All",
    rtlName: "Tier",
    icon: Visible,
    component: Tview,
    layout: "/admin",
  },

  {
    path: "/tcreate",
    name: "Create",
    rtlName: "Tier/create",
    icon: Add,
    component: Tcreate,
    layout: "/admin",
  },

  {
    path: "/tdelete",
    name: "Delete",
    rtlName: "Tier/delete",
    icon: Delete,
    component: Tdelete,
    layout: "/admin",
  },
  {
    path: "/tedit",
    name: "Edit",
    rtlName: "Tier/edit",
    icon: Edit,
    component: Tedit,
    layout: "/admin",
  },
];

export default dashboardRoutes;
