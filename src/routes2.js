import Visible from "@material-ui/icons/VisibilityOutlined";
import Add from "@material-ui/icons/AddCircleOutlineOutlined";
import adminDelete from "views/SiteAdmin/Udelete";
import Ublock from "views/UserSegment/Ublock.js";
import ResetPwd from "views/UserSegment/ResetPwd.js";
import SiteAdmin from "views/SiteAdmin/SiteAdmin.js";
import SiteView from "views/SiteAdmin/ViewAll";
import SiteCreate from "views/SiteAdmin/CreateAdmin";
import SiteEdit from "views/SiteAdmin/EditAdmin";

const dashboardRoutes = [
  {
    path: "/sadmin",
    name: "",
    rtlName: "Site Admin",
    icon: "",
    component: SiteAdmin,
    layout: "/admin",
    display: "none",
  },

  {
    path: "/adminview",
    name: "View All",
    rtlName: "User Segment",
    icon: Visible,
    component: SiteView,
    layout: "/admin",
  },

  {
    path: "/adminCreate",
    name: "Create",
    rtlName: "User Segment/create",
    icon: Add,
    component: SiteCreate,
    layout: "/admin",
  },

  {
    path: "/adminDelete",
    name: "",
    rtlName: "Site Admin/delete",
    icon: "",
    component: adminDelete,
    layout: "/admin",
  },
  {
    path: "/adminEdit",
    name: "",
    rtlName: "User Segment/edit",
    icon: "",
    component: SiteEdit,
    layout: "/admin",
  },
  {
    path: "/ublock",
    name: "",
    rtlName: "User Segment/block",
    icon: "",
    component: Ublock,
    layout: "/admin",
  },
  {
    path: "/resetpwdd",
    name: "",
    rtlName: "Reset Password",
    icon: "",
    component: ResetPwd,
    layout: "/admin",
  },
];

export default dashboardRoutes;
