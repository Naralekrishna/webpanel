import Visible from "@material-ui/icons/VisibilityOutlined";
import Add from "@material-ui/icons/AddCircleOutlineOutlined";
import { AdView } from "views/Advertisement/AdView";
import { AdCreate } from "views/Advertisement/AdCreate";
import { AdEdit } from "views/Advertisement/AdEdit";

const Image = require("./assets/img/video-advertising.png");

const dashboardRoutes = [
  {
    path: "/advt",
    name: "Advertisement",
    rtlName: "Advertisement",
    icon: Image,
    component: AdView,
    layout: "/admin",
  },
  {
    path: "/adview",
    name: "View All",
    rtlName: "Advertisement",
    icon: Visible,
    component: AdView,
    layout: "/admin",
  },

  {
    path: "/adcreate",
    name: "Add",
    rtlName: "Advertisement",
    icon: Add,
    component: AdCreate,
    layout: "/admin",
  },

  //   {
  //     path: "/ad",
  //     name: "Delete",
  //     rtlName: "Rewards",
  //     icon: Delete,
  //     component: Rdel,
  //     layout: "/admin"
  //   },
  {
    path: "/Adedit",
    name: "",
    rtlName: "Advertisement",
    icon: "",
    component: AdEdit,
    layout: "/admin",
  },
];

export default dashboardRoutes;
