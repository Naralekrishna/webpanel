import Createe from "views/NUserseg/Createe";
import Deactivatee from "views/NUserseg/Deactivatee";
import { Edit } from "views/NUserseg/Editt";
import Add from "@material-ui/icons/AddCircleOutlineOutlined";
import Visible from "@material-ui/icons/VisibilityOutlined";
import View from "views/NUserseg/view";
const dashboardRoutes = [
  {
    path: "/view",
    name: "View All",
    rtlName: "",
    icon: Visible,
    component: View,
    layout: "/admin",
  },
  {
    path: "/create",
    name: "Create",
    rtlName: "لوحة القيادة",
    icon: Add,
    component: Createe,
    layout: "/admin",
  },

  {
    path: "/deact",
    name: "",
    rtlName: "لوحة القيادة",
    icon: "",
    component: Deactivatee,
    layout: "/admin",
  },

  {
    path: "/ediit",
    name: "",
    rtlName: "لوحة القيادة",
    icon: "",
    component: Edit,
    layout: "/admin",
  },
];

export default dashboardRoutes;
