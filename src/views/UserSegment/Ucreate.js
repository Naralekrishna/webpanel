import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Button from "components/CustomButtons/Button.js";
import { Modal } from "react-bootstrap";
import "../../stylee.css";
import "./css.js";
import { FormControl, MenuItem, Select, TextField } from "@material-ui/core";
import { addUser } from "utils/Services";
import { Redirect } from "react-router-dom";
import { getStates } from "utils/Services";

const IndianState = [
  { name: "Andaman and Nicobar Islands" },
  { name: "Andhra Pradesh" },
  { name: "Arunachal Pradesh" },
  { name: "Assam" },
  { name: "Bihar" },
  { name: "Chandigarh" },
  { name: "Chhattisgarh" },
  { name: "Dadra and Nagar Haveli" },
  { name: "Daman and Diu" },
  { name: "Delhi" },
  { name: "Goa" },
  { name: "Gujarat" },
  { name: "Haryana" },
  { name: "Himachal Pradesh" },
  { name: "Jammu and Kashmir" },
  { name: "Jharkhand" },
  { name: "Karnataka" },
  { name: "Kerala" },
  { name: "Ladakh" },
  { name: "Lakshadweep" },
  { name: "Madhya Pradesh" },
  { name: "Maharashtra" },
  { name: "Manipur" },
  { name: "Meghalaya" },
  { name: "Mizoram" },
  { name: "Nagaland" },
  { name: "Odisha" },
  { name: "Puducherry" },
  { name: "Punjab" },
  { name: "Rajasthan" },
  { name: "Sikkim" },
  { name: "Tamil Nadu" },
  { name: "Telangana" },
  { name: "Tripura" },
  { name: "Uttar Pradesh" },
  { name: "Uttarakhand" },
  { name: "West Bengal" },
];

export default function Ucreate() {
  const useStyles = makeStyles();
  const classes = useStyles();
  const [show, setShow] = useState(false);
  const [name, setName] = useState("");
  const [segment, setSegment] = useState("");
  const [email, setEmail] = useState("");
  const [gender, setGender] = useState("");
  const [mobile, setMobile] = useState("");
  const [errorShow, setErrorShow] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

  const [city, setCity] = useState("");
  const [state, setState] = useState("");
  const [pincode, setPincode] = useState("");
  const [dob, setDob] = useState("2017-05-24");
  const [houseNo, setHouseNo] = useState("");
  const [redirect, setRedirect] = useState(false);
  const [stateData, setStateData] = useState("");
  const [country, setCountry] = useState("");
  const [clinicName, setClinicName] = useState("");
  const [clinicAddress, setClinicAddress] = useState("");
  const [clinicRegisterationNumber, setClinicRegisterationNumber] = useState(
    ""
  );
  const [clinicGstNumber, setClinicGstNumber] = useState("");
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");
  const [type, setType] = useState("");
  const [referedBy, setReferedBy] = useState("");
  const [userData, setUserData] = useState(sessionStorage.getItem("payload"));
  const newPayload = JSON.parse(userData);
  useEffect(() => {
    getStates().then((res) => {
      setStateData(res.data.States);
    });
  }, []);

  const handleChange = (event) => {
    if (event.target.name === "Name") {
      setName(event.target.value);
    } else if (event.target.name === "Segment") {
      setSegment(event.target.value);
    } else if (event.target.name === "Email") {
      setEmail(event.target.value);
    } else if (event.target.name === "Gender") {
      setGender(event.target.value);
    } else if (event.target.name === "MobileNo") {
      setMobile(event.target.value);
    } else if (event.target.name === "City") {
      setCity(event.target.value);
    } else if (event.target.name === "State") {
      setState(event.target.value);
    } else if (event.target.name === "Pincode") {
      setPincode(event.target.value);
    } else if (event.target.name === "DOB") {
      setDob(event.target.value);
    } else if (event.target.name === "Country") {
      setCountry(event.target.value);
    } else if (event.target.name === "ClinicName") {
      setClinicName(event.target.value);
    } else if (event.target.name === "ClinicAddress") {
      setClinicAddress(event.target.value);
    } else if (event.target.name === "ClinicRegisterationNumber") {
      setClinicRegisterationNumber(event.target.value);
    } else if (event.target.name === "ClinicGstNumber") {
      setClinicGstNumber(event.target.value);
    } else if (event.target.name === "StartDate") {
      setStartDate(event.target.value);
    } else if (event.target.name === "EndDate") {
      setEndDate(event.target.value);
    } else if (event.target.name === "Type") {
      setType(event.target.value);
    } else if (event.target.name === "ReferedBy") {
      setReferedBy(event.target.value);
    } else {
      setHouseNo(event.target.value);
    }
  };

  const handleClose = () => setShow(false);

  const clearFunction = () => {
    setName("");
    setSegment("");
    setEmail("");
    setGender("");
    setMobile("");
    setPincode("");
    setCity("");
    setDob("");
    setHouseNo("");
    setState("");
    setCountry("");
    setClinicName("");
    setClinicAddress("");
    setClinicRegisterationNumber("");
    setClinicGstNumber("");
    setStartDate("");
    setEndDate("");
    setType("");
    setReferedBy("");
  };

  const RedirectToView = () => {
    setRedirect(true);
  };

  if (redirect) {
    return <Redirect to="/admin/useruserview" />;
  }
  const onSubmit = () => {
    const data = {
      Name: name,
      Email: email,
      Gender: gender,
      MobileNo: mobile,
      City: city,
      State: state,
      Pincode: pincode,
      DOB: dob,
      HouesNo: houseNo,
      Address: "NA",
      Country: country,
      ClinicName: clinicName,
      ClinicAddress: clinicAddress,
      ClinicRegistrationNumber: clinicRegisterationNumber,
      ClinicGstNumber: clinicGstNumber,
      StartDate: startDate,
      EndDate: endDate,
      Type: type,
      ReferedBy: referedBy,
      UserId: newPayload.UserProfile.UserId,
    };

    addUser(data)
      .then((res) => {
        if (res.data.success === "200") {
          setShow(true);
        } else {
          setErrorShow(true);
          setErrorMessage(res.data.message);
        }
      })
      .catch((err) => {
        setErrorShow(true);
        setErrorMessage(err.response.data.message);
      });
  };
  if (redirect) {
    return <Redirect to="/admin/useruserview" />;
  }
  return (
    <div style={{ height: "100vh", width: "100%" }}>
      {errorShow && (
        <Modal show={errorShow} onHide={() => setErrorShow(false)}>
          <Modal.Header closeButton>
            <Modal.Title>Error Occured</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {errorMessage ? errorMessage : "Fill credentials Properly"}
          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" onClick={() => setErrorShow(false)}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      )}
      {
        <Modal show={show} onHide={() => setShow(false)}>
          <Modal.Header closeButton>
            <Modal.Title>Success</Modal.Title>
          </Modal.Header>
          <Modal.Body>Successfully User Created</Modal.Body>
          <Modal.Footer>
            {/* <Link to="/admin/useruserview"> */}
            <Button
              style={{ color: "white", backgroundColor: "green" }}
              variant="primary"
              onClick={() => RedirectToView()}
            >
              Close
            </Button>
            {/* </Link> */}
          </Modal.Footer>
        </Modal>
      }

      <div style={{ marginLeft: 15 }}>
        <div style={{ fontSize: 20, fontWeight: "bold", lineHeight: 4 }}>
          {" "}
          Personal Information
        </div>

        <div
          style={{
            width: 30,
            height: 2,
            backgroundColor: "#bf891b",
            marginTop: -30,
          }}
        ></div>
      </div>
      <div
        className="row m-3 p-4"
        style={{
          backgroundColor: "white",
          display: "flex",
          flexDirection: "row",
          marginTop: 40,
        }}
      >
        <div></div>
        <div className="col-lg-4 col-sm-12" style={{ marginRight: 0 }}>
          <div style={{ marginLeft: 15 }}>Name</div>

          <input
            value={name}
            name="Name"
            onChange={(e) => handleChange(e)}
            className="col-lg-12 col-sm-12"
            type="text"
            placeholder="First Name and Last Name"
            style={{
              paddingLeft: 10,
              fontSize: 15,
              background: "transparent",
              borderStyle: "solid",
              borderWidth: 1,
              borderColor: "#bf891b",
              height: 40,
              borderRadius: 40,
              marginBottom: 30,
            }}
          />
        </div>

        <div className="col-lg-4 col-sm-12" style={{ marginRight: 0 }}>
          <span style={{ marginLeft: 15 }}>Date Of Birth</span>
          <div
            style={{
              background: "transparent",
              borderStyle: "solid",
              borderWidth: 1,
              borderColor: "#bf891b",
              height: 40,
              borderRadius: 40,
              marginBottom: 30,
            }}
          >
            <form className={classes.container} noValidate>
              <TextField
                id="date"
                type="date"
                name="DOB"
                value={dob}
                onChange={(e) => handleChange(e)}
                defaultValue="2017-05-24"
                style={{
                  width: "100%",
                  paddingRight: "15px",
                  paddingLeft: "15px",
                }}
                InputProps={{
                  disableUnderline: true,
                }}
              />
            </form>
            {/* <div style={{marginLeft:10, fontSize:15, lineHeight:2.5}}> 20/06/1998</div> 
					<div style={{marginLeft:'auto', padding:4, paddingRight:8}}>   <DateRangeOutlined /> </div>		 */}
            {/* </div> */}
          </div>
        </div>

        <div className="col-lg-4 col-sm-12 " style={{ marginRight: 0 }}>
          <div style={{ marginLeft: 15 }}>Gender</div>
          <div
            style={{
              background: "transparent",
              borderStyle: "solid",
              borderWidth: 1,
              borderColor: "#bf891b",
              height: 40,
              borderRadius: 40,
              marginBottom: 30,
            }}
          >
            <FormControl style={{ minWidth: "100%", padding: "5px" }}>
              {/* <InputLabel id="demo-simple-select-outlined-label"></InputLabel> */}
              <Select
                labelId="demo-simple-select-outlined-label"
                id="demo-simple-select-outlined"
                value={gender}
                onChange={handleChange}
                name="Gender"
                disableUnderline={true}
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                <MenuItem value="Male">Male</MenuItem>
                <MenuItem value="Female">Female</MenuItem>
              </Select>
            </FormControl>
          </div>
          {/* <input className='col-lg-12 col-sm-12'
          name="Gender"
          onChange={handleChange}
          type="text" placeholder="Male" style={{ paddingLeft: 10, marginBottom: 20, fontSize: 15, background: 'transparent', borderStyle: 'solid', borderWidth: 1, borderColor: '#bf891b', height: 40, borderRadius: 40, }} /> */}
        </div>

        <div className="col-lg-4 col-sm-12 " style={{ marginRight: 0 }}>
          <div style={{ marginLeft: 15 }}>Mobile No.</div>

          <input
            type="number"
            value={mobile}
            onChange={handleChange}
            name="MobileNo"
            className="col-lg-12 col-sm-12"
            type="text"
            placeholder="+91 9876543210"
            style={{
              paddingLeft: 10,
              marginBottom: 15,
              fontSize: 15,
              background: "transparent",
              borderStyle: "solid",
              borderWidth: 1,
              borderColor: "#bf891b",
              height: 40,
              borderRadius: 40,
            }}
          />
        </div>
        <div className="col-lg-4 col-sm-12" style={{ marginRight: 0 }}>
          <div style={{ marginLeft: 15 }}>Email ID</div>

          <input
            value={email}
            name="Email"
            onChange={handleChange}
            className="col-lg-12 col-sm-12"
            type="text"
            placeholder="abc@gmail.com"
            style={{
              paddingLeft: 10,
              marginBottom: 15,
              fontSize: 15,
              background: "transparent",
              borderStyle: "solid",
              borderWidth: 1,
              borderColor: "#bf891b",
              height: 40,
              borderRadius: 40,
            }}
          />
        </div>
      </div>

      <div style={{ marginLeft: 15 }}>
        <div style={{ fontSize: 20, fontWeight: "bold", lineHeight: 4 }}>
          {" "}
          Address Details
        </div>

        <div
          style={{
            width: 30,
            height: 2,
            backgroundColor: "#bf891b",
            marginTop: -30,
          }}
        ></div>
      </div>
      <div
        className="row m-3 p-4 "
        style={{
          backgroundColor: "white",
          display: "flex",
          flexDirection: "row",
          marginTop: 40,
        }}
      >
        <div className="col-lg-4 col-sm-12" style={{ marginRight: 0 }}>
          <div style={{ marginLeft: 15 }}>House Number / Street Name</div>

          <input
            value={houseNo}
            name="HouseNo"
            onChange={handleChange}
            className="col-lg-12 col-sm-12"
            type="text"
            placeholder="172/2 Ghandhi Marg"
            style={{
              paddingLeft: 10,
              marginBottom: 30,
              fontSize: 15,
              background: "transparent",
              borderStyle: "solid",
              borderWidth: 1,
              borderColor: "#bf891b",
              height: 40,
              borderRadius: 40,
            }}
          />
        </div>

        <div className="col-lg-4 col-sm-12" style={{ marginRight: 0 }}>
          <span style={{ marginLeft: 15 }}>City</span>
          {/* <div style={{ background: 'transparent', marginBottom: 30, borderStyle: 'solid', borderWidth: 1, borderColor: '#bf891b', height: 40, borderRadius: 40, }} > */}

          <div style={{ display: "flex", flexDirection: "row" }}>
            <input
              className="col-lg-12 col-sm-12"
              value={city}
              name="City"
              onChange={(e) => handleChange(e)}
              type="text"
              placeholder="Abc"
              style={{
                paddingLeft: 10,
                marginBottom: 30,
                fontSize: 15,
                background: "transparent",
                borderStyle: "solid",
                borderWidth: 1,
                borderColor: "#bf891b",
                height: 40,
                borderRadius: 40,
              }}
            />

            <div style={{ marginLeft: "auto", padding: 6 }}> </div>
          </div>
          {/* </div> */}
        </div>

        <div className="col-lg-4 col-sm-12" style={{ marginRight: 0 }}>
          <span style={{ marginLeft: 15 }}>State</span>
          <div
            style={{
              background: "transparent",
              marginBottom: 30,
              borderStyle: "solid",
              borderWidth: 1,
              borderColor: "#bf891b",
              height: 40,
              borderRadius: 40,
            }}
          >
            {/* <div style={{ display: 'flex', flexDirection: 'row' }}>
            <input className='col-lg-12 col-sm-12'
              name="State"
              value={state}
              onChange={handleChange}
              type="text" placeholder=""
              style={{ paddingLeft: 10, marginBottom: 30, fontSize: 15, background: 'transparent', borderStyle: 'solid', borderWidth: 1, borderColor: '#bf891b', height: 40, borderRadius: 40, }} /> */}

            {/* <div style={{ marginLeft: 10, fontSize: 15, lineHeight: 2.5 }}> Maharashtra</div>
              <div style={{ marginLeft: 'auto', padding: 6 }}>   	<ExpandMore /> </div> */}
            {/* </div> */}
            <FormControl
              style={{ minWidth: "100%", padding: "5px", paddingLeft: "10px" }}
            >
              {/* <InputLabel id="demo-simple-select-outlined-label"></InputLabel> */}
              <Select
                labelId="demo-simple-select-outlined-label"
                id="demo-simple-select-outlined"
                value={state}
                onChange={handleChange}
                name="State"
                disableUnderline={true}
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                {IndianState &&
                  IndianState.map((res) => {
                    return <MenuItem value={res.name}>{res.name}</MenuItem>;
                  })}
              </Select>
            </FormControl>
          </div>
        </div>

        <div className="col-lg-4 col-sm-12 " style={{ marginRight: 0 }}>
          <span style={{ marginLeft: 15 }}>Country</span>
          <div
            style={{
              background: "transparent",
              marginBottom: 15,
              borderStyle: "solid",
              borderWidth: 1,
              borderColor: "#bf891b",
              height: 40,
              borderRadius: 40,
            }}
          >
            <div style={{ display: "flex", flexDirection: "row" }}>
              <FormControl style={{ minWidth: "100%", padding: "5px" }}>
                {/* <InputLabel id="demo-simple-select-outlined-label"></InputLabel> */}
                <Select
                  labelId="demo-simple-select-outlined-label"
                  id="demo-simple-select-outlined"
                  value={country}
                  onChange={handleChange}
                  name="Country"
                  disableUnderline={true}
                >
                  <MenuItem value="">
                    <em>None</em>
                  </MenuItem>
                  <MenuItem value="India">India</MenuItem>
                </Select>
              </FormControl>
            </div>
            {/* </div> */}
          </div>
        </div>

        <div className="col-lg-4 col-sm-12" style={{ marginRight: 0 }}>
          <div style={{ marginLeft: 15 }}>Pin Code</div>

          <input
            value={pincode}
            name="Pincode"
            onChange={handleChange}
            className="col-lg-12 col-sm-12"
            type="text"
            placeholder="411007"
            style={{
              paddingLeft: 10,
              marginBottom: 15,
              fontSize: 15,
              background: "transparent",
              borderStyle: "solid",
              borderWidth: 1,
              borderColor: "#bf891b",
              height: 40,
              borderRadius: 40,
            }}
          />
        </div>
        <div className="col-lg-4 col-sm-12 " style={{ marginRight: 0 }}></div>
      </div>
      <div style={{ marginLeft: 15 }}>
        <div style={{ fontSize: 20, fontWeight: "bold", lineHeight: 4 }}>
          {" "}
          Clinic Details
        </div>

        <div
          style={{
            width: 30,
            height: 2,
            backgroundColor: "#bf891b",
            marginTop: -30,
          }}
        ></div>
      </div>
      <div
        className="row m-3 p-4 "
        style={{
          backgroundColor: "white",
          display: "flex",
          flexDirection: "row",
          marginTop: 40,
        }}
      >
        <div className="col-lg-4 col-sm-12" style={{ marginRight: 0 }}>
          <div style={{ marginLeft: 15 }}>Name</div>

          <input
            value={clinicName}
            name="ClinicName"
            onChange={handleChange}
            className="col-lg-12 col-sm-12"
            type="text"
            placeholder="172/2 Ghandhi Marg"
            style={{
              paddingLeft: 10,
              marginBottom: 30,
              fontSize: 15,
              background: "transparent",
              borderStyle: "solid",
              borderWidth: 1,
              borderColor: "#bf891b",
              height: 40,
              borderRadius: 40,
            }}
          />
        </div>
        <div className="col-lg-4 col-sm-12" style={{ marginRight: 0 }}>
          <div style={{ marginLeft: 15 }}>Address</div>

          <input
            value={clinicAddress}
            name="ClinicAddress"
            onChange={handleChange}
            className="col-lg-12 col-sm-12"
            type="text"
            placeholder="172/2 Ghandhi Marg"
            style={{
              paddingLeft: 10,
              marginBottom: 30,
              fontSize: 15,
              background: "transparent",
              borderStyle: "solid",
              borderWidth: 1,
              borderColor: "#bf891b",
              height: 40,
              borderRadius: 40,
            }}
          />
        </div>
        <div className="col-lg-4 col-sm-12" style={{ marginRight: 0 }}>
          <div style={{ marginLeft: 15 }}>Registration Number</div>

          <input
            value={clinicRegisterationNumber}
            name="ClinicRegisterationNumber"
            onChange={handleChange}
            className="col-lg-12 col-sm-12"
            type="text"
            placeholder="172/2 Ghandhi Marg"
            style={{
              paddingLeft: 10,
              marginBottom: 30,
              fontSize: 15,
              background: "transparent",
              borderStyle: "solid",
              borderWidth: 1,
              borderColor: "#bf891b",
              height: 40,
              borderRadius: 40,
            }}
          />
        </div>
        <div className="col-lg-4 col-sm-12" style={{ marginRight: 0 }}>
          <div style={{ marginLeft: 15 }}>GST Number</div>

          <input
            value={clinicGstNumber}
            name="ClinicGstNumber"
            onChange={handleChange}
            className="col-lg-12 col-sm-12"
            type="text"
            placeholder="172/2 Ghandhi Marg"
            style={{
              paddingLeft: 10,
              marginBottom: 30,
              fontSize: 15,
              background: "transparent",
              borderStyle: "solid",
              borderWidth: 1,
              borderColor: "#bf891b",
              height: 40,
              borderRadius: 40,
            }}
          />
        </div>

        <div className="col-lg-4 col-sm-12 " style={{ marginRight: 0 }}></div>
      </div>
      <div style={{ marginLeft: 15 }}>
        <div style={{ fontSize: 20, fontWeight: "bold", lineHeight: 4 }}>
          {" "}
          Subscription Details
        </div>

        <div
          style={{
            width: 30,
            height: 2,
            backgroundColor: "#bf891b",
            marginTop: -30,
          }}
        ></div>
      </div>
      <div
        className="row m-3 p-4 "
        style={{
          backgroundColor: "white",
          display: "flex",
          flexDirection: "row",
          marginTop: 40,
        }}
      >
        <div className="col-lg-4 col-sm-12" style={{ marginRight: 0 }}>
          <span style={{ marginLeft: 15 }}>Start Date</span>
          <div
            style={{
              background: "transparent",
              borderStyle: "solid",
              borderWidth: 1,
              borderColor: "#bf891b",
              height: 40,
              borderRadius: 40,
              marginBottom: 30,
            }}
          >
            <form className={classes.container} noValidate>
              <TextField
                id="startDate"
                type="date"
                name="StartDate"
                value={startDate}
                onChange={(e) => handleChange(e)}
                defaultValue="2017-05-24"
                style={{
                  width: "100%",
                  paddingRight: "15px",
                  paddingLeft: "15px",
                }}
                InputProps={{
                  disableUnderline: true,
                }}
              />
            </form>
          </div>
        </div>
        <div className="col-lg-4 col-sm-12" style={{ marginRight: 0 }}>
          <span style={{ marginLeft: 15 }}>End Date</span>
          <div
            style={{
              background: "transparent",
              borderStyle: "solid",
              borderWidth: 1,
              borderColor: "#bf891b",
              height: 40,
              borderRadius: 40,
              marginBottom: 30,
            }}
          >
            <form className={classes.container} noValidate>
              <TextField
                id="endDate"
                type="date"
                name="EndDate"
                value={endDate}
                onChange={(e) => handleChange(e)}
                defaultValue="2017-05-24"
                style={{
                  width: "100%",
                  paddingRight: "15px",
                  paddingLeft: "15px",
                }}
                InputProps={{
                  disableUnderline: true,
                }}
              />
            </form>
          </div>
        </div>
        <div className="col-lg-4 col-sm-12" style={{ marginRight: 0 }}>
          <span style={{ marginLeft: 15 }}>Subscription Type</span>
          <div
            style={{
              background: "transparent",
              marginBottom: 30,
              borderStyle: "solid",
              borderWidth: 1,
              borderColor: "#bf891b",
              height: 40,
              borderRadius: 40,
            }}
          >
            <FormControl
              style={{ minWidth: "100%", padding: "5px", paddingLeft: "10px" }}
            >
              <Select
                labelId="demo-simple-select-outlined-label"
                id="demo-simple-select-outlined"
                value={type}
                onChange={handleChange}
                name="Type"
                disableUnderline={true}
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                <MenuItem value="Free Trial">Free Trial</MenuItem>
                <MenuItem value="One Month">One Month</MenuItem>
                <MenuItem value="Six Months">Six Months</MenuItem>
                <MenuItem value="One Year">One Year</MenuItem>
              </Select>
            </FormControl>
          </div>
        </div>

        <div className="col-lg-4 col-sm-12 " style={{ marginRight: 0 }}></div>
      </div>
      <div style={{ marginLeft: 15 }}>
        <div style={{ fontSize: 20, fontWeight: "bold", lineHeight: 4 }}>
          {" "}
          Refered By
        </div>

        <div
          style={{
            width: 30,
            height: 2,
            backgroundColor: "#bf891b",
            marginTop: -30,
          }}
        ></div>
      </div>
      <div
        className="row m-3 p-4 "
        style={{
          backgroundColor: "white",
          display: "flex",
          flexDirection: "row",
          marginTop: 40,
        }}
      >
        <div className="col-lg-4 col-sm-12" style={{ marginRight: 0 }}>
          <span style={{ marginLeft: 15 }}>Name</span>
          <div
            style={{
              background: "transparent",
              marginBottom: 30,
              borderStyle: "solid",
              borderWidth: 1,
              borderColor: "#bf891b",
              height: 40,
              borderRadius: 40,
            }}
          >
            <FormControl
              style={{ minWidth: "100%", padding: "5px", paddingLeft: "10px" }}
            >
              <Select
                labelId="demo-simple-select-outlined-label"
                id="demo-simple-select-outlined"
                value={referedBy}
                onChange={handleChange}
                name="ReferedBy"
                disableUnderline={true}
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                <MenuItem value="RajYug Solutions">RajYug Solutions</MenuItem>
              </Select>
            </FormControl>
          </div>
        </div>

        <div className="col-lg-4 col-sm-12 " style={{ marginRight: 0 }}></div>
      </div>

      <div
        onClick={onSubmit}
        class="col-lg-12 col-sm-12"
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "flex-end",
          marginTop: 30,
          marginBottom: 20,
        }}
      >
        <div
          className="gradd"
          style={{
            paddingBottom: 30,
            width: 140,
            height: 35,
            borderWidth: 1,
            borderColor: "black",
            zIndex: 5,
            borderRadius: 30,
            borderStyle: "solid",
          }}
        >
          <div
            // onClick={() => onSubmit()}
            style={{
              fontSize: 15,
              fontWeight: "bolder",
              color: "white",
              letterSpacing: 10,
              marginLeft: 40,
              marginTop: 5,
            }}
          >
            {" "}
            CREATE
          </div>
        </div>
        <div
          className="delete"
          onClick={RedirectToView}
          style={{
            paddingBottom: 30,
            width: 140,
            height: 35,
            borderWidth: 1,
            borderColor: "black",
            zIndex: 5,
            borderRadius: 30,
            borderStyle: "solid",
            backgroundColor: "white",
            marginLeft: 30,
          }}
        >
          <div
            style={{
              fontSize: 15,
              fontWeight: "bolder",
              color: "black",
              letterSpacing: 10,
              marginLeft: 40,
              marginTop: 5,
            }}
          >
            {" "}
            CANCEL
          </div>
        </div>
      </div>
    </div>
  );
}
