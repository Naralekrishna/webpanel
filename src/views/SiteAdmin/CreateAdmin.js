import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Button from "components/CustomButtons/Button.js";
import { Modal } from "react-bootstrap";
import "../../stylee.css";
import "./css.js";
import { FormControl, MenuItem, Select, TextField } from "@material-ui/core";
import { Redirect } from "react-router-dom";
import { getStates } from "utils/Services";
import { createSiteAdmin } from "utils/Services";

const IndianState = [
  { name: "Andaman and Nicobar Islands" },
  { name: "Andhra Pradesh" },
  { name: "Arunachal Pradesh" },
  { name: "Assam" },
  { name: "Bihar" },
  { name: "Chandigarh" },
  { name: "Chhattisgarh" },
  { name: "Dadra and Nagar Haveli" },
  { name: "Daman and Diu" },
  { name: "Delhi" },
  { name: "Goa" },
  { name: "Gujarat" },
  { name: "Haryana" },
  { name: "Himachal Pradesh" },
  { name: "Jammu and Kashmir" },
  { name: "Jharkhand" },
  { name: "Karnataka" },
  { name: "Kerala" },
  { name: "Ladakh" },
  { name: "Lakshadweep" },
  { name: "Madhya Pradesh" },
  { name: "Maharashtra" },
  { name: "Manipur" },
  { name: "Meghalaya" },
  { name: "Mizoram" },
  { name: "Nagaland" },
  { name: "Odisha" },
  { name: "Puducherry" },
  { name: "Punjab" },
  { name: "Rajasthan" },
  { name: "Sikkim" },
  { name: "Tamil Nadu" },
  { name: "Telangana" },
  { name: "Tripura" },
  { name: "Uttar Pradesh" },
  { name: "Uttarakhand" },
  { name: "West Bengal" },
];

export default function SiteCreate() {
  const useStyles = makeStyles();
  const classes = useStyles();
  const [show, setShow] = useState(false);
  const [name, setName] = useState("");
  const [segment, setSegment] = useState("");
  const [email, setEmail] = useState("");
  const [gender, setGender] = useState("");
  const [mobile, setMobile] = useState("");
  const [errorShow, setErrorShow] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

  const [city, setCity] = useState("");
  const [state, setState] = useState("");
  const [pincode, setPincode] = useState("");
  const [dob, setDob] = useState("2017-05-24");
  const [redirect, setRedirect] = useState(false);
  const [stateData, setStateData] = useState("");
  const [country, setCountry] = useState("");
  const [userId, setUserId] = useState("");
  const [password, setPassword] = useState("");
  const [role, setRole] = useState("");
  const [houseNo, setHouseNo] = useState("");
  const [userData, setUserData] = useState(sessionStorage.getItem("payload"));
  const newPayload = JSON.parse(userData);
  useEffect(() => {
    getStates().then((res) => {
      console.log(res.data.States);
      setStateData(res.data.States);
    });
  }, []);

  const handleChange = (event) => {
    if (event.target.name === "Name") {
      console.log(event.target.name);
      setName(event.target.value);
    } else if (event.target.name === "Segment") {
      console.log(event.target.name);

      setSegment(event.target.value);
    } else if (event.target.name === "Email") {
      console.log(event.target.name);

      setEmail(event.target.value);
    } else if (event.target.name === "Gender") {
      console.log(event.target.name);

      setGender(event.target.value);
    } else if (event.target.name === "MobileNo") {
      console.log(event.target.name);

      setMobile(event.target.value);
    } else if (event.target.name === "City") {
      console.log(event.target.name);

      setCity(event.target.value);
    } else if (event.target.name === "HouseNo") {
      console.log(event.target.name);

      setHouseNo(event.target.value);
    } else if (event.target.name === "State") {
      console.log(event.target.name);

      setState(event.target.value);
    } else if (event.target.name === "Pincode") {
      console.log(event.target.name);

      setPincode(event.target.value);
    } else if (event.target.name === "DOB") {
      console.log(event.target.name);
      setDob(event.target.value);
    } else if (event.target.name === "Country") {
      console.log(event.target.name);
      setCountry(event.target.value);
    } else if (event.target.name === "UserId") {
      console.log(event.target.name);
      setUserId(event.target.value);
    } else if (event.target.name === "Password") {
      console.log(event.target.name);
      setPassword(event.target.value);
    } else if (event.target.name === "Role") {
      console.log(event.target.name);
      setRole(event.target.value);
    } else {
      setHouseNo(event.target.value);
    }
  };

  const handleClose = () => setShow(false);

  const clearFunction = () => {
    setName("");
    setSegment("");
    setEmail("");
    setGender("");
    setMobile("");
    setPincode("");
    setCity("");
    setDob("");
    setHouseNo("");
    setState("");
    setCountry("");
    setUserId("");
    setPassword("");
    setRole("");
  };

  const RedirectToView = () => {
    setRedirect(true);
  };

  if (redirect) {
    return <Redirect to="/admin/adminview" />;
  }
  const onSubmit = () => {
    const data = {
      Name: name,
      Email: email,
      Gender: gender,
      MobileNo: mobile,
      City: city,
      State: state,
      Country: "NA",
      Pincode: pincode,
      DOB: dob,
      HouesNo: houseNo,
      Password: password,
      Role: role,
      UserId: userId,
    };
    console.log(data);
    createSiteAdmin(data)
      .then((res) => {
        console.log(res);
        if (res.data.success === "200") {
          setShow(true);
        } else {
          setErrorShow(true);
          setErrorMessage(res.data.message);
        }
      })
      .catch((err) => {
        console.log(err);
        setErrorShow(true);
        console.log(err.response.data.message);
        setErrorMessage(err.response.data.message);
      });
  };
  if (redirect) {
    return <Redirect to="/admin/adminview" />;
  }
  return (
    <div style={{ height: "100vh", width: "100%" }}>
      {errorShow && (
        <Modal show={errorShow} onHide={() => setErrorShow(false)}>
          <Modal.Header closeButton>
            <Modal.Title>Error Occured</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {errorMessage ? errorMessage : "Fill credentials Properly"}
          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" onClick={() => setErrorShow(false)}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      )}
      {
        <Modal show={show} onHide={() => setShow(false)}>
          <Modal.Header closeButton>
            <Modal.Title>Success</Modal.Title>
          </Modal.Header>
          <Modal.Body>Successfully Admin Created</Modal.Body>
          <Modal.Footer>
            {/* <Link to="/admin/useruserview"> */}
            <Button
              style={{ color: "white", backgroundColor: "green" }}
              variant="primary"
              onClick={() => RedirectToView()}
            >
              Close
            </Button>
            {/* </Link> */}
          </Modal.Footer>
        </Modal>
      }

      <div style={{ marginLeft: 15 }}>
        <div style={{ fontSize: 20, fontWeight: "bold", lineHeight: 4 }}>
          {" "}
          Personal Information
        </div>

        <div
          style={{
            width: 30,
            height: 2,
            backgroundColor: "#bf891b",
            marginTop: -30,
          }}
        ></div>
      </div>
      <div
        className="row m-3 p-4"
        style={{
          backgroundColor: "white",
          display: "flex",
          flexDirection: "row",
          marginTop: 40,
        }}
      >
        <div></div>
        <div className="col-lg-4 col-sm-12" style={{ marginRight: 0 }}>
          <div style={{ marginLeft: 15 }}>Name</div>

          <input
            value={name}
            name="Name"
            onChange={(e) => handleChange(e)}
            className="col-lg-12 col-sm-12"
            type="text"
            placeholder=""
            style={{
              paddingLeft: 10,
              fontSize: 15,
              background: "transparent",
              borderStyle: "solid",
              borderWidth: 1,
              borderColor: "#bf891b",
              height: 40,
              borderRadius: 40,
              marginBottom: 30,
            }}
          />
        </div>
        <div className="col-lg-4 col-sm-12" style={{ marginRight: 0 }}>
          <div style={{ marginLeft: 15 }}>Email ID</div>

          <input
            value={email}
            name="Email"
            onChange={handleChange}
            className="col-lg-12 col-sm-12"
            type="text"
            placeholder="abc@gmail.com"
            style={{
              paddingLeft: 10,
              marginBottom: 15,
              fontSize: 15,
              background: "transparent",
              borderStyle: "solid",
              borderWidth: 1,
              borderColor: "#bf891b",
              height: 40,
              borderRadius: 40,
            }}
          />
        </div>

        <div className="col-lg-4 col-sm-12" style={{ marginRight: 0 }}>
          <span style={{ marginLeft: 15 }}>Date Of Birth</span>
          <div
            style={{
              background: "transparent",
              borderStyle: "solid",
              borderWidth: 1,
              borderColor: "#bf891b",
              height: 40,
              borderRadius: 40,
              marginBottom: 30,
            }}
          >
            <form className={classes.container} noValidate>
              <TextField
                id="date"
                type="date"
                name="DOB"
                value={dob}
                onChange={(e) => handleChange(e)}
                defaultValue="2017-05-24"
                style={{
                  width: "100%",
                  paddingRight: "15px",
                  paddingLeft: "15px",
                }}
                InputProps={{
                  disableUnderline: true,
                }}
              />
            </form>
          </div>
        </div>

        <div className="col-lg-4 col-sm-12 " style={{ marginRight: 0 }}>
          <div style={{ marginLeft: 15 }}>Gender</div>
          <div
            style={{
              background: "transparent",
              borderStyle: "solid",
              borderWidth: 1,
              borderColor: "#bf891b",
              height: 40,
              borderRadius: 40,
              marginBottom: 30,
            }}
          >
            <FormControl style={{ minWidth: "100%", padding: "5px" }}>
              {/* <InputLabel id="demo-simple-select-outlined-label"></InputLabel> */}
              <Select
                labelId="demo-simple-select-outlined-label"
                id="demo-simple-select-outlined"
                value={gender}
                onChange={handleChange}
                name="Gender"
                disableUnderline={true}
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                <MenuItem value="Male">Male</MenuItem>
                <MenuItem value="Female">Female</MenuItem>
              </Select>
            </FormControl>
          </div>
          {/* <input className='col-lg-12 col-sm-12'
          name="Gender"
          onChange={handleChange}
          type="text" placeholder="Male" style={{ paddingLeft: 10, marginBottom: 20, fontSize: 15, background: 'transparent', borderStyle: 'solid', borderWidth: 1, borderColor: '#bf891b', height: 40, borderRadius: 40, }} /> */}
        </div>

        <div className="col-lg-4 col-sm-12 " style={{ marginRight: 0 }}>
          <div style={{ marginLeft: 15 }}>Mobile No.</div>

          <input
            type="number"
            value={mobile}
            onChange={handleChange}
            name="MobileNo"
            className="col-lg-12 col-sm-12"
            type="text"
            placeholder="+91 9876543210"
            style={{
              paddingLeft: 10,
              marginBottom: 15,
              fontSize: 15,
              background: "transparent",
              borderStyle: "solid",
              borderWidth: 1,
              borderColor: "#bf891b",
              height: 40,
              borderRadius: 40,
            }}
          />
        </div>
        <div className="col-lg-4 col-sm-12" style={{ marginRight: 0 }}></div>
      </div>

      <div style={{ marginLeft: 15 }}>
        <div style={{ fontSize: 20, fontWeight: "bold", lineHeight: 4 }}>
          {" "}
          Address Details
        </div>

        <div
          style={{
            width: 30,
            height: 2,
            backgroundColor: "#bf891b",
            marginTop: -30,
          }}
        ></div>
      </div>
      <div
        className="row m-3 p-4 "
        style={{
          backgroundColor: "white",
          display: "flex",
          flexDirection: "row",
          marginTop: 40,
        }}
      >
        <div className="col-lg-4 col-sm-12" style={{ marginRight: 0 }}>
          <div style={{ marginLeft: 15 }}>Address</div>

          <input
            value={houseNo}
            name="HouseNo"
            onChange={handleChange}
            className="col-lg-12 col-sm-12"
            type="text"
            placeholder="172/2 Ghandhi Marg"
            style={{
              paddingLeft: 10,
              marginBottom: 30,
              fontSize: 15,
              background: "transparent",
              borderStyle: "solid",
              borderWidth: 1,
              borderColor: "#bf891b",
              height: 40,
              borderRadius: 40,
            }}
          />
        </div>

        <div className="col-lg-4 col-sm-12" style={{ marginRight: 0 }}>
          <span style={{ marginLeft: 15 }}>City</span>

          <div style={{ display: "flex", flexDirection: "row" }}>
            <input
              className="col-lg-12 col-sm-12"
              value={city}
              name="City"
              onChange={(e) => handleChange(e)}
              type="text"
              placeholder="City"
              style={{
                paddingLeft: 10,
                marginBottom: 30,
                fontSize: 15,
                background: "transparent",
                borderStyle: "solid",
                borderWidth: 1,
                borderColor: "#bf891b",
                height: 40,
                borderRadius: 40,
              }}
            />

            <div style={{ marginLeft: "auto", padding: 6 }}> </div>
          </div>
          {/* </div> */}
        </div>

        <div className="col-lg-4 col-sm-12" style={{ marginRight: 0 }}>
          <span style={{ marginLeft: 15 }}>State</span>
          <div
            style={{
              background: "transparent",
              marginBottom: 30,
              borderStyle: "solid",
              borderWidth: 1,
              borderColor: "#bf891b",
              height: 40,
              borderRadius: 40,
            }}
          >
            <FormControl
              style={{ minWidth: "100%", padding: "5px", paddingLeft: "10px" }}
            >
              {/* <InputLabel id="demo-simple-select-outlined-label"></InputLabel> */}
              <Select
                labelId="demo-simple-select-outlined-label"
                id="demo-simple-select-outlined"
                value={state}
                onChange={handleChange}
                name="State"
                disableUnderline={true}
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                {IndianState &&
                  IndianState.map((res) => {
                    return <MenuItem value={res.name}>{res.name}</MenuItem>;
                  })}
              </Select>
            </FormControl>
          </div>
        </div>
        <div className="col-lg-4 col-sm-12" style={{ marginRight: 0 }}>
          <div style={{ marginLeft: 15 }}>Pin Code</div>

          <input
            value={pincode}
            name="Pincode"
            onChange={handleChange}
            className="col-lg-12 col-sm-12"
            type="text"
            placeholder="411007"
            style={{
              paddingLeft: 10,
              marginBottom: 15,
              fontSize: 15,
              background: "transparent",
              borderStyle: "solid",
              borderWidth: 1,
              borderColor: "#bf891b",
              height: 40,
              borderRadius: 40,
            }}
          />
        </div>
        <div className="col-lg-4 col-sm-12 " style={{ marginRight: 0 }}></div>
      </div>
      <div style={{ marginLeft: 15 }}>
        <div style={{ fontSize: 20, fontWeight: "bold", lineHeight: 4 }}>
          {" "}
          Login Details
        </div>

        <div
          style={{
            width: 30,
            height: 2,
            backgroundColor: "#bf891b",
            marginTop: -30,
          }}
        ></div>
      </div>
      <div
        className="row m-3 p-4 "
        style={{
          backgroundColor: "white",
          display: "flex",
          flexDirection: "row",
          marginTop: 40,
        }}
      >
        <div className="col-lg-4 col-sm-12" style={{ marginRight: 0 }}>
          <div style={{ marginLeft: 15 }}>User Id</div>

          <input
            value={userId}
            name="UserId"
            onChange={handleChange}
            className="col-lg-12 col-sm-12"
            type="text"
            placeholder="User Id"
            style={{
              paddingLeft: 10,
              marginBottom: 30,
              fontSize: 15,
              background: "transparent",
              borderStyle: "solid",
              borderWidth: 1,
              borderColor: "#bf891b",
              height: 40,
              borderRadius: 40,
            }}
          />
        </div>
        <div className="col-lg-4 col-sm-12" style={{ marginRight: 0 }}>
          <div style={{ marginLeft: 15 }}>Password</div>

          <input
            value={password}
            name="Password"
            onChange={handleChange}
            className="col-lg-12 col-sm-12"
            type="text"
            placeholder="Passowrd"
            style={{
              paddingLeft: 10,
              marginBottom: 30,
              fontSize: 15,
              background: "transparent",
              borderStyle: "solid",
              borderWidth: 1,
              borderColor: "#bf891b",
              height: 40,
              borderRadius: 40,
            }}
          />
        </div>

        <div className="col-lg-4 col-sm-12" style={{ marginRight: 0 }}>
          <span style={{ marginLeft: 15 }}>User Type</span>
          <div
            style={{
              background: "transparent",
              marginBottom: 30,
              borderStyle: "solid",
              borderWidth: 1,
              borderColor: "#bf891b",
              height: 40,
              borderRadius: 40,
            }}
          >
            <FormControl
              style={{ minWidth: "100%", padding: "5px", paddingLeft: "10px" }}
            >
              <Select
                labelId="demo-simple-select-outlined-label"
                id="demo-simple-select-outlined"
                value={role}
                onChange={handleChange}
                name="Role"
                disableUnderline={true}
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                <MenuItem value="Admin">Admin</MenuItem>
                <MenuItem value="Super Admin">Super Admin</MenuItem>
                <MenuItem value="User">User</MenuItem>
              </Select>
            </FormControl>
          </div>
        </div>
        <div className="col-lg-4 col-sm-12 " style={{ marginRight: 0 }}></div>
      </div>

      <div
        onClick={onSubmit}
        class="col-lg-12 col-sm-12"
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "flex-end",
          marginTop: 30,
          marginBottom: 20,
        }}
      >
        <div
          className="gradd"
          style={{
            paddingBottom: 30,
            width: 140,
            height: 35,
            borderWidth: 1,
            borderColor: "black",
            zIndex: 5,
            borderRadius: 30,
            borderStyle: "solid",
          }}
        >
          <div
            // onClick={() => onSubmit()}
            style={{
              fontSize: 15,
              fontWeight: "bolder",
              color: "white",
              letterSpacing: 10,
              marginLeft: 40,
              marginTop: 5,
            }}
          >
            {" "}
            CREATE
          </div>
        </div>
        <div
          className="delete"
          onClick={RedirectToView}
          style={{
            paddingBottom: 30,
            width: 140,
            height: 35,
            borderWidth: 1,
            borderColor: "black",
            zIndex: 5,
            borderRadius: 30,
            borderStyle: "solid",
            backgroundColor: "white",
            marginLeft: 30,
          }}
        >
          <div
            style={{
              fontSize: 15,
              fontWeight: "bolder",
              color: "black",
              letterSpacing: 10,
              marginLeft: 40,
              marginTop: 5,
            }}
          >
            {" "}
            CANCEL
          </div>
        </div>
      </div>
    </div>
  );
}
