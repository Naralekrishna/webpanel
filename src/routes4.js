import Visible from "@material-ui/icons/VisibilityOutlined";
import Add from "@material-ui/icons/AddCircleOutlineOutlined";
import Redeem from "@material-ui/icons/Redeem";
import Rewards from "views/Rewards/RewardsViewAll";
import RewardsAdd from "views/Rewards/RewardsAdd.js";
import RewardsViewAll from "views/Rewards/RewardsViewAll.js";
import Rdel from "views/Rewards/Rdel.js";
import { RewardsEdit } from "views/Rewards/Redit";

const dashboardRoutes = [
  {
    path: "/rewards",
    name: "Rewards",
    rtlName: "Rewards",
    icon: Redeem,
    component: Rewards,
    layout: "/admin",
  },
  {
    path: "/Rewardsview",
    name: "View All",
    rtlName: "Rewards",
    icon: Visible,
    component: RewardsViewAll,
    layout: "/admin",
  },

  {
    path: "/RAdd",
    name: "Add",
    rtlName: "Rewards",
    icon: Add,
    component: RewardsAdd,
    layout: "/admin",
  },

  {
    path: "/Rdel",
    name: "",
    rtlName: "Rewards",
    icon: "",
    component: Rdel,
    layout: "/admin",
  },
  {
    path: "/REdit",
    name: "",
    rtlName: "Rewards",
    icon: "",
    component: RewardsEdit,
    layout: "/admin",
  },
];

export default dashboardRoutes;
