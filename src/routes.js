import Home from "@material-ui/icons/Home";
import DashboardPage from "views/Dashboard/Dashboard.js";

const dashboardRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    rtlName: "لوحة القيادة",
    icon: Home,
    component: DashboardPage,
    layout: "/admin",
  },
];

export default dashboardRoutes;
